class Acronym
  def self.abbreviate(phrase)
    phrase.scan(/\b[a-zA-Z]/).map(&:upcase).join
  end
end
