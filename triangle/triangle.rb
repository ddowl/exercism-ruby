class Triangle
  def initialize(sides)
    @sides = sides
    @valid = sides.max < sides.sum - sides.max
  end

  def equilateral?
    @valid && @sides.uniq.size == 1
  end

  def isosceles?
    @valid && (@sides.uniq.size == 2 || @sides.uniq.size == 1)
  end

  def scalene?
    @valid && @sides.uniq.size == 3
  end
end