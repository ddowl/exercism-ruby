class Grains
  CHESSBOARD = (1..64)

  def self.square(n)
    raise ArgumentError unless CHESSBOARD.cover?(n)
    2 ** (n - 1)
  end

  def self.total
    CHESSBOARD.sum { |day| square(day) }
  end
end