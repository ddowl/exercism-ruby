class ListOps
  def self.arrays(arrs)
    count = 0
    arrs.each do
      count += 1
    end
    count
  end

  def self.reverser(arr)
    rev = []
    arr.each do |item|
      rev.unshift(item)
    end
    rev
  end

  def self.concatter(a, b)
    res = []
    a.each do |item|
      res.push(item)
    end
    b.each do |item|
      res.push(item)
    end
    res
  end

  def self.mapper(arr)
    res = []
    if block_given?
      arr.each do |item|
        res.push(yield(item))
      end
    end
    res
  end

  def self.filterer(arr)
    res = []
    if block_given?
      arr.each do |item|
        if yield(item)
          res.push(item)
        end
      end
    end
    res
  end

  def self.sum_reducer(arr)
    sum = 0
    arr.each do |num|
      sum += num
    end
    sum
  end

  def self.factorial_reducer(arr)
    prod = 1
    arr.each do |num|
      prod *= num
    end
    prod
  end
end
