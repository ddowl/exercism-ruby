class Queens
  BOARD_SIZE = 8

  def initialize(white:, black: nil)
    raise ArgumentError unless on_board?(white[0]) && on_board?(white[1])
    @white = white
    raise ArgumentError unless black.nil? || (on_board?(black[0]) && on_board?(black[1]))
    @black = black
  end

  def attack?
    same_row? || same_column? || same_diag?
  end

  private

  def on_board?(pos)
    pos >= 0 && pos < BOARD_SIZE
  end

  def same_row?
    @white[0] == @black[0]
  end

  def same_column?
    @white[1] == @black[1]
  end

  def same_diag?
    slope = (@white[0] - @black[0]) / (@white[1] - @black[1])
    [1, -1].include?(slope)
  end
end
