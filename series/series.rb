class Series
  def initialize(nums)
    @nums = nums
  end

  # Output all the contiguous substrings of length n in that
  # string in the order that they appear.
  def slices(n)
    raise ArgumentError if n > @nums.size
    (0..@nums.size - n).map { |i| @nums[i...i + n] }
  end
end
