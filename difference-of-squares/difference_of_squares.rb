class Squares
  def initialize(n)
    @one_through_n = (1..n)
  end

  def square_of_sum
    @one_through_n.sum.abs2
  end

  def sum_of_squares
    @one_through_n.map(&:abs2).sum
  end

  def difference
    square_of_sum - sum_of_squares
  end
end
