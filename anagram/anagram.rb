class Anagram
  def initialize(word)
    @word = word.downcase
    @anagram_model = char_hash(word)
  end

  def match(candidates)
    candidates.select { |word| anagram?(word) }
  end

  private

  def anagram?(word)
    @anagram_model == char_hash(word) && @word != word.downcase
  end

  def char_hash(word)
    word.downcase.chars.group_by(&:itself).transform_values(&:count)
  end
end