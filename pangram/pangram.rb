require 'set'

class Pangram
  ALPHA = 'abcdefghijklmnopqrstuvwxyz'.chars.to_set

  def self.pangram?(sentence)
    sentence.downcase.chars.select { |c| /[a-z]/.match(c) }.to_set == ALPHA
  end
end