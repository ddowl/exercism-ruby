class Raindrops
  def self.convert(n)
    sound = ''
    sound += 'Pling' if (n % 3).zero?
    sound += 'Plang' if (n % 5).zero?
    sound += 'Plong' if (n % 7).zero?
    sound.empty? ? n.to_s : sound
  end
end
