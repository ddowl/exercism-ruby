class CollatzConjecture

  # Take any positive integer n. If n is even, divide n by 2 to get n / 2.
  # If n is odd, multiply n by 3 and add 1 to get 3n + 1.
  # Repeat the process until you reach 1!
  def self.steps(n)
    raise ArgumentError if n <= 0

    step = 0
    until n == 1
      if n.even?
        n /= 2
      else
        n = 3 * n + 1
      end
      step += 1
    end
    step
  end
end
