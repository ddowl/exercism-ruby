class RunLengthEncoding
  def self.encode(plaintext)
    plaintext.gsub(/(.)\1+/) { |s| "#{s.length}#{s[0]}" }
  end

  def self.decode(encoding)
    encoding.gsub(/\d+./) { |s| s[-1] * s.to_i }
  end
end
