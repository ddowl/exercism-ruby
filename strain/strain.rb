class Array
  def keep(&block)
    each_with_object([]) do |val, res|
      res << val if yield(val)
    end
  end

  def discard(&block)
    self - keep(&block)
  end
end
