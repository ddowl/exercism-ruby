class Board
  def self.transform(str_board)
    @board = str_board
    validate_board
    build_board(find_mines)
  end

  private

  def self.find_mines
    mines = []
    @board.each_with_index do |line, i|
      col_idxs = (0...line.length).find_all { |i| line[i] == '*' }
      mines += col_idxs.map { |col| [i, col] } unless col_idxs.empty?
    end
    mines
  end

  def self.build_board(mines)
    @board.map.with_index do |line, row|
      line.chars.map.with_index do |char, column|
        if char == ' '
          n = num_mines_adjacent(row, column, mines)
          n.zero? ? ' ' : n
        else
          char
        end
      end.join
    end
  end

  def self.num_mines_adjacent(x, y, mines)
    (adjacent(x, y) & mines).count
  end

  def self.adjacent(x, y)
    [
      [x + 1, y],
      [x - 1, y],
      [x, y + 1],
      [x, y - 1],
      [x + 1, y + 1],
      [x + 1, y - 1],
      [x - 1, y + 1],
      [x - 1, y - 1]
    ]
  end

  def self.validate_board
    raise ArgumentError unless is_big_enough? &&
                               rows_are_same_len? &&
                               has_defined_borders? &&
                               has_valid_chars?
  end

  def self.is_big_enough?
    @board.length >= 3
  end

  def self.rows_are_same_len?
    width = @board.first.length
    @board.all? { |row| row.length == width }
  end

  def self.has_defined_borders?
    @board[1...-1].all? { |row| row[0] == '|' && row[-1] == '|' }
  end

  def self.has_valid_chars?
    @board[1...-1].join !~ /[^\s*|]/
  end
end