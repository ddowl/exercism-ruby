class Nucleotide

  NUCLEOTIDES = ['G','T','A','C']

  def initialize(strand)
    @strand = strand
  end

  def self.from_dna(strand)
    raise ArgumentError if strand =~ /[^#{NUCLEOTIDES}]/

    Nucleotide.new(strand)
  end

  def count(nuc)
    @strand.chars.count(nuc)
  end

  def histogram
    NUCLEOTIDES.map { |nuc| [nuc, count(nuc)] }.to_h
  end
end
