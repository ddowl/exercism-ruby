class Bst
  include Enumerable
  attr_accessor :data, :left, :right

  def initialize(data)
    @data = data
    @left = nil
    @right = nil
  end

  def insert(val)
    if val <= data
      @left = insert_into_subtree(left, val)
    else
      @right = insert_into_subtree(right, val)
    end
    self
  end

  def each(&block)
    return to_enum unless block_given?

    left.each(&block) unless left.nil?
    yield data
    right.each(&block) unless right.nil?
  end

  private

  def insert_into_subtree(node, val)
    if node.nil?
      Bst.new(val)
    else
      node.insert(val)
    end
  end
end
