class Array
  def accumulate
    if block_given?
      res = []
      each do |val|
        res << yield(val)
      end
      res
    else
      to_enum
    end
  end
end
