class Matrix
  attr_reader :rows, :columns

  def initialize(str_rep)
    @rows = str_rep.each_line.map { |line| line.split.map(&:to_i) }
    @columns = @rows.transpose
  end

  def saddle_points
    points = []
    rows.each_with_index do |row, i|
      columns.each_with_index do |col, j|
        num = row[j]
        points << [i, j] if num == row.max && num == col.min
      end
    end
    points
  end
end
