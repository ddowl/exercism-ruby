class Robot
  DIRECTIONS = [:east, :north, :west, :south]

  def orient(direction)
    raise ArgumentError unless DIRECTIONS.include?(direction)
    @direction_idx = DIRECTIONS.index(direction)
  end

  def bearing
    DIRECTIONS[@direction_idx]
  end

  def turn_right
    @direction_idx = (@direction_idx - 1) % DIRECTIONS.length
  end

  def turn_left
    @direction_idx = (@direction_idx + 1) % DIRECTIONS.length
  end

  def at(x, y)
    @x_pos = x
    @y_pos = y
  end

  def coordinates
    [@x_pos, @y_pos]
  end

  def advance
    case bearing
    when :north
      @y_pos += 1
    when :east
      @x_pos += 1
    when :south
      @y_pos -= 1
    when :west
      @x_pos -= 1
    end
  end
end

class Simulator
  def instructions(seq)
    seq.scan(/./).map do |char|
      case char
      when 'L'
        :turn_left
      when 'R'
        :turn_right
      when 'A'
        :advance
      end
    end
  end

  def place(robot, x:, y:, direction:)
    robot.at(x, y)
    robot.orient(direction)
  end

  def evaluate(robot, seq)
    instructions(seq).each do |inst|
      robot.send(inst)
    end
  end
end
