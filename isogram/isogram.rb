require 'set'

class Isogram
  def self.isogram?(word)
    chars = word.downcase.scan(/[^-\s]/)
    chars.to_set.size == chars.size
  end
end
