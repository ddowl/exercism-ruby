class PhoneNumber

  class NanpCleaner
    NUM_DIGITS = 10
    NUM_DIGITS_INTERNATIONAL = 11

    def initialize(number)
      @nanp = number.chars.select { |c| /[[:digit:]]/.match(c) }.join
      @ac = area_code
      @ec = exchange_code
      @ic = international_code
    end

    def nanp
      return nil unless valid_number?
      @nanp[-NUM_DIGITS..-1]
    end

    def valid_number?
      valid_length? && valid_area_code? && valid_exchange_code? &&
        (valid_international_code? || !international?)
    end

    def valid_area_code?
      valid_code?(@ac)
    end

    def valid_exchange_code?
      valid_code?(@ec)
    end

    def valid_code?(code)
      code[0] =~ /[2-9]/ &&
        code[1] =~ /[[:digit:]]/ &&
        code[2] =~ /[[:digit:]]/
    end

    def valid_international_code?
      @ic == '1'
    end

    def valid_length?
      @nanp.length == NUM_DIGITS || @nanp.length == NUM_DIGITS_INTERNATIONAL
    end

    def area_code
      @nanp[-10..-8]
    end

    def exchange_code
      @nanp[-7..-5]
    end

    def international_code
      @nanp[0..-11]
    end

    def international?
      @nanp.length == NUM_DIGITS_INTERNATIONAL
    end
  end

  def self.clean(number)
    NanpCleaner.new(number).nanp
  end
end
