class Phrase
  def initialize(remark)
    @remark = remark.strip
  end

  def silence?
    @remark.empty?
  end

  def question?
    @remark.end_with?('?')
  end

  def yelling?
    letters = @remark.chars.select { |c| /[[:alpha:]]/.match(c) }
    letters.any? && letters.all? { |c| /[[:upper:]]/.match(c) }
  end
end

class Bob
  def self.hey(remark)
    phrase = Phrase.new(remark)
    if phrase.silence?
      'Fine. Be that way!'
    elsif phrase.question? && phrase.yelling?
      "Calm down, I know what I'm doing!"
    elsif phrase.yelling?
      'Whoa, chill out!'
    elsif phrase.question?
      'Sure.'
    else
      'Whatever.'
    end
  end
end
